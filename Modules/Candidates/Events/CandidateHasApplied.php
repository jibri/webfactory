<?php

namespace TypiCMS\Modules\Candidates\Events;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Events\Dispatcher;
use Illuminate\Mail\Message;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Storage;
use TypiCMS\Modules\Users\Models\User;

class CandidateHasApplied
{
    public function onCreate($candidate, $job)
    {
        $webmaster = User::where('superuser', 1)->first();

        Mail::send('candidates::emails.Candidate_apply_to_webmaster', ['candidate' => $candidate, 'job' => $job], function (Message $message) use ($candidate, $webmaster, $job) {
            $subject = $candidate->name . 'has applied for job: ' . $job->name;
            $sender = config('mail.from.address');
            $cv = storage_path("app/cv/{$candidate->cv}");

            $message->from($sender)
                    ->to($webmaster->email)
                    ->subject($subject)
                    ->attach($cv);
        });
    }

    /**
     * Register the listeners for the subscriber.
     *
     * @param Dispatcher $events
     *
     * @return void
     */
    public function subscribe($events)
    {
        $events->listen('CandidateHasApplied', 'TypiCMS\Modules\Candidates\Shells\Events\EventHandler@onCreate');
    }
}