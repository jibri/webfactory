<?php

namespace TypiCMS\Modules\Candidates\Http\Controllers;

use Illuminate\Support\Facades\Response;
use TypiCMS\Modules\Core\Shells\Http\Controllers\BaseAdminController;
use TypiCMS\Modules\Candidates\Shells\Models\Candidate;
use TypiCMS\Modules\Candidates\Shells\Repositories\CandidateInterface;

class AdminController extends BaseAdminController
{
    public function __construct(CandidateInterface $candidate)
    {
        parent::__construct($candidate);
    }

    /**
     * List models.
     *
     * @return \Illuminate\View\View
     */
    public function index()
    {
        $candidates = Candidate::all()->load('jobPositions');

        return view('candidates::admin.index')
            ->with(compact('candidates'));
    }

    /**
     * Show model.
     *
     * @param Candidate $candidate
     * @return \Illuminate\View\View
     */
    public function show(Candidate $candidate)
    {
        $candidate->load('jobpositions');

        return view('candidates::admin.show')
            ->with(compact('candidate'));
    }

    /**
     * Download CV.
     *
     * @param Candidate $candidate
     * @return \Illuminate\Http\Response;
     */
    public function downloadCV(Candidate $candidate)
    {
        $cv = storage_path("app/cv/{$candidate->cv}");
        if (!file_exists($cv))
            exit('Requested file does not exist on server!');

        return Response::download($cv);
    }

}
