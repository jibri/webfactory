<?php

namespace TypiCMS\Modules\Candidates\Http\Controllers;

use Illuminate\Support\Facades\Storage;
use TypiCMS\Modules\Candidates\Models\Candidate;
use TypiCMS\Modules\Core\Shells\Http\Controllers\BasePublicController;
use TypiCMS\Modules\Candidates\Shells\Repositories\CandidateInterface;
use TypiCMS\Modules\Jobpositions\Models\Jobposition;
use TypiCMS\Modules\Candidates\Http\Requests\FormRequest;

class PublicController extends BasePublicController
{
    public function __construct(CandidateInterface $candidate)
    {
        parent::__construct($candidate);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index()
    {
        $models = $this->repository->all();

        return view('candidates::public.index')
            ->with(compact('models'));
    }

    /**
     * Show news.
     *
     * @return \Illuminate\View\View
     */
    public function show($slug)
    {
        $model = $this->repository->bySlug($slug);

        return view('candidates::public.show')
            ->with(compact('model'));
    }

    /**
     * Store model for candidate
     *
     * @param FormRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(FormRequest $request)
    {
        $filename = str_random(5) . '-' . $request->cv->getClientOriginalName();
        $content = file_get_contents($request->file('cv')->getRealPath());
        Storage::disk('local')->put("cv/{$filename}", $content);

        $job = Jobposition::find($request->jobpositions_id);

        $candidate = $job->candidates()->create([
            'name' => strip_tags($request->name),
            'email' => strip_tags($request->email),
            'comments' => strip_tags($request->comments),
            'cv' => $filename
        ]);

        event('CandidateHasApplied', [$candidate, $job]);

        session()->flash('success', 'You have successfully applied for job!');

        return redirect()->back();
    }
}
