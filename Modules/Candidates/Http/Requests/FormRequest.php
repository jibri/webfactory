<?php

namespace TypiCMS\Modules\Candidates\Http\Requests;

use TypiCMS\Modules\Core\Shells\Http\Requests\AbstractFormRequest;

class FormRequest extends AbstractFormRequest
{
    public function rules()
    {
        return [
            'name' => 'required|max:255',
            'email'  => 'required|email|max:255',
            'comments' => 'required',
            'cv'  => 'required|mimes:pdf,doc,docx,odt',
            'jobposition_id' => 'exists:jobpositions,id'
        ];
    }
}
