<?php

namespace TypiCMS\Modules\Candidates\Models;

use Laracasts\Presenter\PresentableTrait;
use TypiCMS\Modules\Core\Shells\Models\Base;
use TypiCMS\Modules\History\Shells\Traits\Historable;
use TypiCMS\Modules\Jobpositions\Models\Jobposition;

class Candidate extends Base
{
    use Historable;
    use PresentableTrait;

    protected $presenter = 'TypiCMS\Modules\Candidates\Shells\Presenters\ModulePresenter';

    /**
     * Declare any properties that should be hidden from JSON Serialization.
     *
     * @var array
     */
    protected $hidden = [];

    protected $guarded = [];

    /**
     * Create relation to Job position
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function jobPositions()
    {
        return $this->belongsTo(Jobposition::class, 'jobposition_id');
    }

}
