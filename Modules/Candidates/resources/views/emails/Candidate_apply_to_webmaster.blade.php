<!DOCTYPE html>
<html lang="{{ config('app.locale') }}">

<head>
    <meta charset="utf-8">
</head>

<body>

    <p>Name: {{ $candidate->name }}</p>
    <p>Email: {{ $candidate->email }}</p>
    <p>Applied for job: {{ $job->title }}</p>
    <p>Comments:{{ $candidate->comments }}</p>

</body>

</html>