<?php

namespace TypiCMS\Modules\Jobpositions\Composers;

use Illuminate\Contracts\View\View;
use Illuminate\Support\Facades\Gate;
use Maatwebsite\Sidebar\SidebarGroup;
use Maatwebsite\Sidebar\SidebarItem;

class SidebarViewComposer
{
    public function compose(View $view)
    {
        $view->sidebar->group(trans('global.menus.content'), function (SidebarGroup $group) {
            $group->addItem(trans('jobpositions::global.name'), function (SidebarItem $item) {
                $item->id = 'jobpositions';
                $item->icon = config('typicms.jobpositions.sidebar.icon');
                $item->weight = config('typicms.jobpositions.sidebar.weight');
                $item->route('admin::index-jobpositions');
                $item->append('admin::create-jobposition');
                $item->authorize(
                    Gate::allows('index-jobpositions')
                );
            });
        });
    }
}
