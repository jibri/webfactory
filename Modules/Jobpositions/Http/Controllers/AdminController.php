<?php

namespace TypiCMS\Modules\Jobpositions\Http\Controllers;

use TypiCMS\Modules\Core\Shells\Http\Controllers\BaseAdminController;
use TypiCMS\Modules\Jobpositions\Shells\Http\Requests\FormRequest;
use TypiCMS\Modules\Jobpositions\Shells\Models\Jobposition;
use TypiCMS\Modules\Jobpositions\Shells\Repositories\JobpositionInterface;

class AdminController extends BaseAdminController
{
    public function __construct(JobpositionInterface $jobposition)
    {
        parent::__construct($jobposition);
    }

    /**
     * List models.
     *
     * @return \Illuminate\View\View
     */
    public function index()
    {
        return view('jobpositions::admin.index');
    }

    /**
     * Create form for a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        $model = $this->repository->getModel();

        return view('jobpositions::admin.create')
            ->with(compact('model'));
    }

    /**
     * Edit form for the specified resource.
     *
     * @param \TypiCMS\Modules\Jobpositions\Shells\Models\Jobposition $jobposition
     *
     * @return \Illuminate\View\View
     */
    public function edit(Jobposition $jobposition)
    {
        return view('jobpositions::admin.edit')
            ->with(['model' => $jobposition]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \TypiCMS\Modules\Jobpositions\Shells\Http\Requests\FormRequest $request
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(FormRequest $request)
    {
        $jobposition = $this->repository->create($request->all());

        return $this->redirect($request, $jobposition);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \TypiCMS\Modules\Jobpositions\Shells\Models\Jobposition            $jobposition
     * @param \TypiCMS\Modules\Jobpositions\Shells\Http\Requests\FormRequest $request
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(Jobposition $jobposition, FormRequest $request)
    {
        $this->repository->update($request->all());

        return $this->redirect($request, $jobposition);
    }
}
