<?php

namespace TypiCMS\Modules\Jobpositions\Http\Controllers;

use TypiCMS\Modules\Candidates\Models\Candidate;
use TypiCMS\Modules\Core\Shells\Http\Controllers\BasePublicController;
use TypiCMS\Modules\Jobpositions\Shells\Repositories\JobpositionInterface;

class PublicController extends BasePublicController
{
    public function __construct(JobpositionInterface $jobposition)
    {
        parent::__construct($jobposition);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index()
    {
        $models = $this->repository->all();

        return view('jobpositions::public.index')
            ->with(compact('models'));
    }

    /**
     * Show news.
     *
     * @return \Illuminate\View\View
     */
    public function show($slug)
    {
        $model = $this->repository->bySlug($slug);

        return view('jobpositions::public.show')
            ->with(compact('model'));
    }

    public function create(Candidate $candidate)
    {

    }
}
