<?php

namespace TypiCMS\Modules\Jobpositions\Models;

use TypiCMS\Modules\Core\Shells\Models\BaseTranslation;

class JobpositionTranslation extends BaseTranslation
{
    /**
     * get the parent model.
     */
    public function owner()
    {
        return $this->belongsTo('TypiCMS\Modules\Jobpositions\Shells\Models\Jobposition', 'jobposition_id')->withoutGlobalScopes();
    }
}
