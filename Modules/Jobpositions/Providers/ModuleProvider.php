<?php

namespace TypiCMS\Modules\Jobpositions\Providers;

use Illuminate\Foundation\AliasLoader;
use Illuminate\Foundation\Application;
use Illuminate\Support\ServiceProvider;
use TypiCMS\Modules\Core\Shells\Facades\TypiCMS;
use TypiCMS\Modules\Core\Shells\Observers\FileObserver;
use TypiCMS\Modules\Core\Shells\Observers\SlugObserver;
use TypiCMS\Modules\Core\Shells\Services\Cache\LaravelCache;
use TypiCMS\Modules\Jobpositions\Shells\Models\Jobposition;
use TypiCMS\Modules\Jobpositions\Shells\Models\JobpositionTranslation;
use TypiCMS\Modules\Jobpositions\Shells\Repositories\CacheDecorator;
use TypiCMS\Modules\Jobpositions\Shells\Repositories\EloquentJobposition;

class ModuleProvider extends ServiceProvider
{
    public function boot()
    {
        $this->mergeConfigFrom(
            __DIR__.'/../config/config.php', 'typicms.jobpositions'
        );

        $modules = $this->app['config']['typicms']['modules'];
        $this->app['config']->set('typicms.modules', array_merge(['jobpositions' => ['linkable_to_page', 'srcDir' => __DIR__.'/../']], $modules));

        $this->loadViewsFrom(__DIR__.'/../resources/views/', 'jobpositions');
        $this->loadTranslationsFrom(__DIR__.'/../resources/lang', 'jobpositions');

        $this->publishes([
            __DIR__.'/../resources/views' => base_path('resources/views/vendor/jobpositions'),
        ], 'views');
        $this->publishes([
            __DIR__.'/../database' => base_path('database'),
        ], 'migrations');

        AliasLoader::getInstance()->alias(
            'Jobpositions',
            'TypiCMS\Modules\Jobpositions\Shells\Facades\Facade'
        );

        // Observers
        JobpositionTranslation::observe(new SlugObserver());
        Jobposition::observe(new FileObserver());
    }

    public function register()
    {
        $app = $this->app;

        /*
         * Register route service provider
         */
        $app->register('TypiCMS\Modules\Jobpositions\Shells\Providers\RouteServiceProvider');

        /*
         * Sidebar view composer
         */
        $app->view->composer('core::admin._sidebar', 'TypiCMS\Modules\Jobpositions\Shells\Composers\SidebarViewComposer');

        /*
         * Add the page in the view.
         */
        $app->view->composer('jobpositions::public.*', function ($view) {
            $view->page = TypiCMS::getPageLinkedToModule('jobpositions');
        });

        $app->bind('TypiCMS\Modules\Jobpositions\Shells\Repositories\JobpositionInterface', function (Application $app) {
            $repository = new EloquentJobposition(new Jobposition());
            if (!config('typicms.cache')) {
                return $repository;
            }
            $laravelCache = new LaravelCache($app['cache'], 'jobpositions', 10);

            return new CacheDecorator($repository, $laravelCache);
        });
    }
}
