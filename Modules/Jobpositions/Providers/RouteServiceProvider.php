<?php

namespace TypiCMS\Modules\Jobpositions\Providers;

use Illuminate\Routing\Router;
use TypiCMS\Modules\Core\Shells\Facades\TypiCMS;
use TypiCMS\Modules\Core\Shells\Providers\BaseRouteServiceProvider as ServiceProvider;

class RouteServiceProvider extends ServiceProvider
{
    /**
     * This namespace is applied to the controller routes in your routes file.
     *
     * In addition, it is set as the URL generator's root namespace.
     *
     * @var string
     */
    protected $namespace = 'TypiCMS\Modules\Jobpositions\Shells\Http\Controllers';

    /**
     * Define the routes for the application.
     *
     * @param \Illuminate\Routing\Router $router
     *
     * @return void
     */
    public function map(Router $router)
    {
        $router->group(['namespace' => $this->namespace], function (Router $router) {

            /*
             * Front office routes
             */
            if ($page = TypiCMS::getPageLinkedToModule('jobpositions')) {
                $options = $page->private ? ['middleware' => 'auth'] : [];
                foreach (config('translatable.locales') as $lang) {
                    if ($page->translate($lang)->status && $uri = $page->uri($lang)) {
                        $router->get($uri, $options + ['as' => $lang.'.jobpositions', 'uses' => 'PublicController@index']);
                        $router->get($uri.'/{slug}', $options + ['as' => $lang.'.jobpositions.slug', 'uses' => 'PublicController@show']);
                    }
                }
            }

            /*
             * Admin routes
             */
            $router->get('admin/jobpositions', 'AdminController@index')->name('admin::index-jobpositions');
            $router->get('admin/jobpositions/create', 'AdminController@create')->name('admin::create-jobposition');
            $router->get('admin/jobpositions/{jobposition}/edit', 'AdminController@edit')->name('admin::edit-jobposition');
            $router->post('admin/jobpositions', 'AdminController@store')->name('admin::store-jobposition');
            $router->put('admin/jobpositions/{jobposition}', 'AdminController@update')->name('admin::update-jobposition');

            /*
             * API routes
             */
            $router->get('api/jobpositions', 'ApiController@index')->name('api::index-jobpositions');
            $router->put('api/jobpositions/{jobposition}', 'ApiController@update')->name('api::update-jobposition');
            $router->delete('api/jobpositions/{jobposition}', 'ApiController@destroy')->name('api::destroy-jobposition');
        });
    }
}
