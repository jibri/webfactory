<?php

namespace TypiCMS\Modules\Jobpositions\Repositories;

use Illuminate\Database\Eloquent\Model;
use TypiCMS\Modules\Core\Shells\Repositories\RepositoriesAbstract;

class EloquentJobposition extends RepositoriesAbstract implements JobpositionInterface
{
    public function __construct(Model $model)
    {
        $this->model = $model;
    }
}
