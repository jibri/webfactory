<?php

namespace TypiCMS\Modules\Jobpositions\Repositories;

use TypiCMS\Modules\Core\Shells\Repositories\RepositoryInterface;

interface JobpositionInterface extends RepositoryInterface
{
}
