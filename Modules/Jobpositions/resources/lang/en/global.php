<?php

return [
    'name'     => 'Jobpositions',
    'jobpositions'  => 'jobposition|jobpositions',
    'New'      => 'New jobposition',
    'Edit'     => 'Edit jobposition',
    'Back'     => 'Back to jobpositions',
];
