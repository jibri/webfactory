<?php

return [
    'name'     => 'Objetos',
    'jobpositions'  => 'objeto|objetos',
    'New'      => 'Nuevo objeto',
    'Edit'     => 'Editar objeto',
    'Back'     => 'Volver a los objetos',
];
