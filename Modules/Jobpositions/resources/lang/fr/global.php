<?php

return [
    'name'     => 'Jobpositions',
    'jobpositions'  => 'jobposition|jobpositions',
    'New'      => 'Nouveau jobposition',
    'Edit'     => 'Modifier jobposition',
    'Back'     => 'Retour à la liste des jobpositions',
];
