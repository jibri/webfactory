# Installation

- clone repo
- create database
- run `composer install`
- run `php artisan admintool:install`
- run `php artisan key:generate`

**Backend:**

- create job positions in admin
- create page for jobs and attach job positions module
- setup mail driver
- go to the job listing page, click a job, apply for it

**Frontend:**

- apply for job