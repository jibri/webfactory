@extends('core::admin.master')

@section('title', trans('candidates::global.name'))

@section('main')

    <h1>
        <span>{{ $candidates->count() }} {{ str_plural('Candidate', $candidates->count()) }}</span>
    </h1>

    <div class="btn-toolbar">
        @include('core::admin._lang-switcher')
    </div>

    <table class="table table-responsive">
        <tr>
            <th>name</th>
            <th>email</th>
            <th>applied for</th>
            <th>details</th>
        </tr>
        @foreach($candidates as $candidate)
            <tr>
                <td>{{ $candidate->name }}</td>
                <td>{{ $candidate->email }}</td>
                <td>{{ $candidate->jobPositions->title }}</td>
                <td><a href="{{ route('admin::show-candidate', $candidate) }}">details</a></td>
            </tr>
        @endforeach
    </table>

@endsection
