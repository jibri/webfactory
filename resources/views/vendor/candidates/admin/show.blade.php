@extends('core::admin.master')

@section('title', trans('candidates::global.name'))

@section('main')

    <h1>Candidate details</h1>

    <br>

    <h4>Name: {{ $candidate->name }}</h4>
    <h4>Email: {{ $candidate->email }}</h4>
    <h4>Comments: {{ $candidate->comments }}</h4>

    <br>

    <h3>Applied for:</h3>

    <br>

    <h4>Job title: {{ $candidate->jobpositions->title }}</h4>
    <h4>Job summary: {{ $candidate->jobpositions->summary }}</h4>
    <h4>Download CV: <a href="{{ route('cv.download', $candidate) }}">{{ $candidate->cv }}</a></h4>

@endsection