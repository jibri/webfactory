@extends('core::admin.master')

@section('title', trans('jobpositions::global.New'))

@section('main')

    @include('core::admin._button-back', ['module' => 'jobpositions'])
    <h1>
        @lang('jobpositions::global.New')
    </h1>

    {!! BootForm::open()->action(route('admin::index-jobpositions'))->multipart()->role('form') !!}
        @include('jobpositions::admin._form')
    {!! BootForm::close() !!}

@endsection
