<li>
    <a href="{{ route($lang.'.jobpositions.slug', $jobposition->slug) }}" title="{{ $jobposition->title }}">
        {!! $jobposition->title !!}
        {!! $jobposition->present()->thumb(null, 200) !!}
    </a>
</li>
