@extends('pages::public.master')

@section('bodyClass', 'body-jobpositions body-jobpositions-index body-page body-page-'.$page->id)

@section('main')

    @if($errors->has('job_missing'))
        <h1 style="color: #cccccc; background: #ff0000;">Sorry, the job position was not found!</h1>
    @endif

    {!! $page->present()->body !!}

    @include('galleries::public._galleries', ['model' => $page])

    @if ($models->count())
    @include('jobpositions::public._list', ['items' => $models])
    @endif

@endsection
