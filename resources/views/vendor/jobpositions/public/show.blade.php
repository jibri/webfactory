@extends('core::public.master')

@section('title', $model->title.' – '.trans('jobpositions::global.name').' – '.$websiteTitle)
@section('ogTitle', $model->title)
@section('description', $model->summary)
@section('image', $model->present()->thumbUrl())
@section('bodyClass', 'body-jobpositions body-jobposition-'.$model->id.' body-page body-page-'.$page->id)

@section('main')

    @include('core::public._btn-prev-next', ['module' => 'Jobpositions', 'model' => $model])

    @if(session()->has('success'))
        <h2 style="color: #fff; text-align: center; background: #02d500; padding: 30px;">
            {{ session('success') }}
        </h2>
    @endif

    <article>
        <h1>{{ $model->title }}</h1>
        {!! $model->present()->thumb(null, 200) !!}
        <p class="summary">{{ nl2br($model->summary) }}</p>
        <div class="body">{!! $model->present()->body !!}</div>
    </article>

    <hr>
    <h2>Application form</h2>

    {!! BootForm::open()->action(route('candidates.store'))->multipart() !!}
        <input type="hidden" name="jobpositions_id" value="{{ $model->id }}">
        {!! BootForm::text('name', 'name') !!}
        {!! BootForm::email('email', 'email') !!}
        {!! BootForm::textarea('comments', 'comments') !!}
        {!! BootForm::file('cv', 'cv') !!}
        {!! BootForm::submit('Submit') !!}
    {!! BootForm::close() !!}

@endsection
